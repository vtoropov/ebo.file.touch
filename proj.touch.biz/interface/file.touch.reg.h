#pragma once
/*
	Created by Tech_dog (ebontrop@gmail.com) on 22-Oct-2020 at 4:05:36p, UTC+7, Novosibirsk, Thursday;
	This is File Touch registry wrap interface declaration file;
*/
#include "file.touch.cfg.h"
#include "file.touch.timer.h"
namespace file { namespace touch { namespace biz {

//	using shared::registry::CRegistryStg; typedef CRegistryStg TRegStg;

	class CPers {
	private:
//		TRegStg   m_stg;

	public:
		 CPers (void);
		~CPers (void);

	public:
		CPers&  operator << (const TCfg&);
		CPers&  operator << (const TTimer&);

		CPers&  operator >> (TCfg&);
		CPers&  operator >> (TTimer&);

	private: // non-copyable;
		CPers (const CPers&);
		CPers& operator = (const CPers&);
	};

}}}

typedef file::touch::biz::CPers   TTouchPers;