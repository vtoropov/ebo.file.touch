#ifndef _FILEWDOGGETINF_H_BC0E4462_503C_4148_B806_02C6E9EA52F8_INCLUDED
#define _FILEWDOGGETINF_H_BC0E4462_503C_4148_B806_02C6E9EA52F8_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2020 at 8:32:54p, UTC+7, Novosibirsk, Sunday;
	This is File Touch project watch dog getting file information interface declaration file;
*/
#include "shared.gen.sys.err.h"

#include "file.wdog.com.defs.h"

namespace file { namespace touch { namespace wdog {

	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows/win32/api/fileapi/ns-fileapi-by_handle_file_information
	// https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ne-minwinbase-file_info_by_handle_class
	typedef BY_HANDLE_FILE_INFORMATION  TFileInfo;

	class CFileInfo : public TFileInfo,
	                  public CFile_base {
	                 typedef CFile_base TBase;

	public:
		CFileInfo(void);
		CFileInfo(const CFileInfo&);

	public:
		virtual HRESULT   Set(LPCTSTR _lp_sz_path) override sealed;
	public:
		TSysEvtId     Compare(const CFileInfo&  ) const;
	public:
		CFileInfo& operator= (const CFileInfo&);
		CFileInfo& operator<<(LPCTSTR _lp_sz_path);
	};

	bool operator==(const CFileInfo& _lhs, const CFileInfo& _rhs);
	bool operator!=(const CFileInfo& _lhs, const CFileInfo& _rhs); 

}}}

#endif/*_FILEWDOGGETINF_H_BC0E4462_503C_4148_B806_02C6E9EA52F8_INCLUDED*/