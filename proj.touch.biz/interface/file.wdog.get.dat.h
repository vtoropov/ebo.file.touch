#ifndef _FILEWDOGGETDAT_H_A8127C00_4901_4352_B70F_32171C4809B9_INCLUDED
#define _FILEWDOGGETDAT_H_A8127C00_4901_4352_B70F_32171C4809B9_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2020 at 0:03:29a, UTC+7, Novosibirsk, Monday;
	This is File Touch project watch dog getting file data interface declaration file;
*/
#include "shared.gen.sys.err.h"

#include "file.wdog.com.defs.h"

namespace file { namespace touch { namespace wdog {

	using shared::sys_core::CError;
	// https://docs.microsoft.com/en-us/windows/win32/api/minwinbase/ns-minwinbase-win32_find_dataa
	typedef WIN32_FIND_DATA TFileData;

	class CFileData : public TFileData,
	                  public CFile_base {
	                 typedef CFile_base TBase;
	public:
		 CFileData(void);
		 CFileData(const CFileData&);
		~CFileData(void);
	public:
		virtual HRESULT   Set(LPCTSTR _lp_sz_path) override sealed;
	public:
		TSysEvtId     Compare(const CFileData&  ) const;
	public:
		CFileData& operator= (const CFileData&);
		CFileData& operator<<(LPCTSTR _lp_sz_path);
	};

	bool operator==(const CFileData& _lhs, const CFileData& _rhs);
	bool operator!=(const CFileData& _lhs, const CFileData& _rhs); 

}}}

#endif/*_FILEWDOGGETDAT_H_A8127C00_4901_4352_B70F_32171C4809B9_INCLUDED*/