#ifndef _FILETOUCHCFG_H_9A723487_6558_41C5_B645_69AB915DA044_INCLUDED
#define _FILETOUCHCFG_H_9A723487_6558_41C5_B645_69AB915DA044_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Feb-2020 at 4:29:23p, UTC+7, Novosibirsk, Tuesday;
	This is File Touch project configuration interface declaration file;
*/
#include "shared.gen.cmd.arg.h"
#include "shared.gen.sys.err.h"

namespace file { namespace touch { namespace biz {

	using shared::common::CCommandLine;
	using shared::sys_core::CError;

	class CCfg_Params {
	public:
		enum _mode {
			e_info = 0, // file info class is used for getting file state; (default)
			e_data = 1, // find file data is used for getting file state;
		};
	public:
		static CStringW EnumToText(const _mode);
		static _mode IntToEnum(const INT);
	};

	typedef CCfg_Params        TParams;
	typedef CCfg_Params::_mode TMode;

	class CCfg {
	public:
		enum _defalt {
			e_delay  = 500,      // default delay value for timer, msec;
		};
	protected:
		DWORD       m_dw_msec;   // value of msecs for timer based solution;
		TMode       m_wd_mode;   // watch method mode;
		CStringW    m_cs_path;   // file path that is being monitored;
		mutable
		CError      m_error;

	public:
		 CCfg (void);
		~CCfg (void);

	public:
		DWORD       Delay (void) const ;
		HRESULT     Delay (const DWORD);
		TErrorRef   Error (void) const ;
		const bool  Is    (void) const ;
		const
		TMode&      Mode  (void) const ;
		TMode&      Mode  (void)       ;
		LPCTSTR     Path  (void) const ;
		HRESULT     Path  (LPCTSTR)    ;
		HRESULT     Set   (const CCommandLine&);

	public:
		CCfg& operator =  (const CCommandLine&);
	public:
		static CCfg& GetObject(void); // gets the static configuration object reference; *note* - not thread safe;
	};

}}}

typedef file::touch::biz::CCfg   TCfg;

#endif/*_FILETOUCHCFG_H_9A723487_6558_41C5_B645_69AB915DA044_INCLUDED*/