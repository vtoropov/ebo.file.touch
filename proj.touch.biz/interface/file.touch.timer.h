#ifndef _FILETOUCHTIMER_H_B75CFA75_E2CC_4233_9C6D_BFD4E262EE12_INCLUDED
#define _FILETOUCHTIMER_H_B75CFA75_E2CC_4233_9C6D_BFD4E262EE12_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2020 at 10:58:54a, UTC+7, Novosibirsk, Thursday;
	This is File Touch project timer interface declaration file;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.evt.h"

#include "file.touch.cfg.h"
#include "file.wdog.com.defs.h"
#include "file.wdog.get.inf.h"
#include "file.wdog.get.dat.h"

namespace file { namespace touch { namespace biz {

	using shared::sys_core::CError;
	using shared::runnable::CMarshaller;
	using shared::runnable::CMarshaller;
	using shared::runnable::IGenericEventNotify;
	using shared::runnable::CDelayEvent;

	using file::touch::wdog::CFileInfo;
	using file::touch::wdog::CFileData;
	using file::touch::wdog::TSysEvtId;

	interface ITimerEventSink
	{
		virtual HRESULT ITimer_OnError(const CError&) PURE;
		virtual HRESULT ITimer_OnEvent(const CFileData&) { return E_NOTIMPL; }
		virtual HRESULT ITimer_OnEvent(const CFileInfo&) { return E_NOTIMPL; }
	};

	class CTimer : public IGenericEventNotify {
	private:
		ITimerEventSink*
		          m_p_sink;
		volatile
		bool      m_break_it_on;
		HANDLE    m_thread;
		CError    m_error ;
		HANDLE    m_stop_evt ;
		CMarshaller m_adapter;
		CDelayEvent m_delay  ;

	public:
		 CTimer(ITimerEventSink* = NULL);
		~CTimer(void);

	public:
		const
		CDelayEvent&  Delay(void) const ;
		CDelayEvent&  Delay(void)       ;
		TErrorRef     Error(void) const ;
		bool          IsOn (void) const ;
		VOID          Sink (ITimerEventSink* ); // not thread safe;
		HRESULT       Turn (const bool _b_on );

	private: // IGenericEventNotify
#pragma warning (disable: 4481)
		virtual HRESULT  GenericEvent_OnNotify(const _variant_t v_evt_id) override sealed;
#pragma warning (default: 4481)
	public:
		DWORD Thread_Fun (void);
	public:
		static CTimer& GetObject(void);
	};

}}}

typedef file::touch::biz::CTimer  TTimer;

#endif/*_FILETOUCHTIMER_H_B75CFA75_E2CC_4233_9C6D_BFD4E262EE12_INCLUDED*/