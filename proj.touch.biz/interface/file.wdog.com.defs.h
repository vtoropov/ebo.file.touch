#ifndef _FWWDOGCOMMONDEFS_H_16CFDAA7_FB51_4ebf_B69C_492843CD8D8D_INCLUDED
#define _FWWDOGCOMMONDEFS_H_16CFDAA7_FB51_4ebf_B69C_492843CD8D8D_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jan-2016 at 5:10:04pm, GMT+7, Phuket, Rawai, Sunday;
	This is (thefileguardian.com) watch dog common definition(s) declaration file.
	-----------------------------------------------------------------------------
	Adopted to File Touch project on 6-Feb-2020 at 4:14:35p, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.gen.sys.err.h"
#include "shared.gen.sys.time.h"

namespace file { namespace touch { namespace wdog
{
	using shared::sys_core::CError;
	using shared::lite::common::CDateTime;

	// https://docs.microsoft.com/en-us/windows/win32/api/winbase/nf-winbase-readdirectorychangesw
	class eSystemEvent {
	public:
		enum _id : DWORD {
			eUndefined   = 0x0,
			eFileCreated = FILE_ACTION_ADDED   ,
			eFileDeleted = FILE_ACTION_REMOVED ,
			eFileChanged = FILE_ACTION_MODIFIED,
			eFileRenamed = FILE_ACTION_RENAMED_OLD_NAME|FILE_ACTION_RENAMED_NEW_NAME,
			eFileCopied  =(eFileRenamed + 1)
		};
	public:
		static
		CStringW ToString(const _id);
	};

	typedef eSystemEvent::_id TSysEvtId ;

	class CFile_base {
	protected:
		TSysEvtId   m_event;
		CError      m_error;
		CStringW  m_path ;
		CDateTime   m_stamp; // this is not related to file properties, but is a marker of when they were gotten;

	protected:
		 CFile_base (void);
		~CFile_base (void);

	public:
		virtual
		HRESULT     Set    (LPCTSTR _lp_sz_path) PURE;

	public:
		TSysEvtId   Compare(const CFile_base&  ) const; 
		TErrorRef   Error  (void) const;
		const
		TSysEvtId&  Event  (void) const;
		TSysEvtId&  Event  (void)      ;
		const bool  Is     (LPCTSTR _lp_sz_path) const;
		LPCTSTR     Path   (void) const;
	public:
		const
		CDateTime&  Timestamp(void) const;
		CDateTime&  Timestamp(void)      ;
	public:
		CFile_base& operator= (const CFile_base&);
	};
#if (0)
	// this interface is used from timer-based class work thread as callback for notifications;
	interface IFileSystemEventSink
	{
		virtual HRESULT IFileSystem_OnError(const CError&) PURE;
		virtual HRESULT IFileSystem_OnEvent(const CFileData&) { return E_NOTIMPL; }
		virtual HRESULT IFileSystem_OnEvent(const CFileInfo&) { return E_NOTIMPL; }
	};

	class CFile_compare {
	public:
		 CFile_compare(void);
		~CFile_compare(void);

	public:
		TSysEvtId  Data(const CFileData& _lh_ref, const CFileData& _rh_ref) const;
		TSysEvtId  Info(const CFileInfo& _lh_ref, const CFileInfo& _rh_ref) const;
	
	};
#endif
}}}

#endif/*_FWWDOGCOMMONDEFS_H_16CFDAA7_FB51_4ebf_B69C_492843CD8D8D_INCLUDED*/