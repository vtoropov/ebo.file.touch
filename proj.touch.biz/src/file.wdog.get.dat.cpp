/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2020 at 0:27:02a, UTC+7, Novosibirsk, Monday;
	This is File Touch project watch dog getting file data interface implementation file;
*/
#include "StdAfx.h"
#include "file.wdog.get.dat.h"

using namespace file::touch::wdog;

#include "shared.gen.han.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

CFileData:: CFileData(void) : TBase() { ::memset((TFileData*)this, 0, sizeof(TFileData));}
CFileData:: CFileData(const CFileData& _ref) : TBase() { *this = _ref; }
CFileData::~CFileData(void) {}

/////////////////////////////////////////////////////////////////////////////

TSysEvtId    CFileData::Compare(const CFileData&  _rh_ref) const {

	TSysEvtId evt_id = TBase::Compare(_rh_ref);

	if (evt_id != TSysEvtId::eUndefined)
		return evt_id;
	if (this->Error() && _rh_ref.Error())
		return evt_id;

	if (::CompareFileTime(&this->ftCreationTime ,&_rh_ref.ftCreationTime ) != 0)
		return (evt_id = TSysEvtId::eFileCreated);
	if (::CompareFileTime(&this->ftLastWriteTime,&_rh_ref.ftLastWriteTime) != 0) 
		return (evt_id = TSysEvtId::eFileChanged);

	if (this->nFileSizeLow  != _rh_ref.nFileSizeLow ||
		this->nFileSizeHigh != _rh_ref.nFileSizeHigh)
		evt_id = TSysEvtId::eFileChanged;

	return evt_id;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CFileData::Set    (LPCTSTR _lp_sz_path) {
	m_error << __MODULE__ << S_OK;

	m_stamp.Refresh();

	if (NULL == _lp_sz_path || 0 == ::_tcslen(_lp_sz_path))
		return ((m_error = E_INVALIDARG) = _T("File path cannot be empty;"));
	else
		m_path =_lp_sz_path;

	HANDLE h_found = ::FindFirstFile(_lp_sz_path, this);
	if (h_found == INVALID_HANDLE_VALUE)
		return (m_error = ::GetLastError());

	::FindClose(h_found); h_found = NULL;

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CFileData& CFileData::operator= (const CFileData& _ref) {
	(CFile_base&)*this = (CFile_base&)_ref;

	const errno_t err_ = ::memcpy_s((TFileData*)this, sizeof(TFileData), (TFileData*)&_ref, sizeof(TFileData));
	if (0 != err_) {
		(this->m_error = E_OUTOFMEMORY) = _T("Copying data is failed;");
	}
	return *this;
}

CFileData& CFileData::operator<<(LPCTSTR _lp_sz_path) {
	this->Set(_lp_sz_path);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch { namespace wdog {

bool operator==(const CFileData& _lhs, const CFileData& _rhs) {
	return TSysEvtId::eUndefined == _lhs.Compare(_rhs);
}
bool operator!=(const CFileData& _lhs, const CFileData& _rhs) {
	return TSysEvtId::eUndefined != _lhs.Compare(_rhs);
}

}}}