/*
	Created by Tech_dog (ebontrop@gmail.com) on 4-Feb-2020 at 4:59:25p, UTC+7, Novosibirsk, Tuesday;
	This is File Touch project configuration interface implementation file;
*/
#include "StdAfx.h"
#include "file.touch.cfg.h"

using namespace file::touch::biz;
/////////////////////////////////////////////////////////////////////////////

CStringW CCfg_Params::EnumToText(const _mode _e_value) {

	CStringW cs_mode;
	switch (_e_value) {
	case _mode::e_data: { cs_mode = _T("Find_Data"); } break;
	case _mode::e_info: { cs_mode = _T("File_Info"); } break;
	default:
		cs_mode = _T("#undef");
	}

	return cs_mode;
}

TMode CCfg_Params::IntToEnum(const INT _n_value) {

	TMode e_mode = TMode::e_info;
	switch (_n_value) {
	case 1: { e_mode = TMode::e_data; } break;
	}
	return e_mode;
}

/////////////////////////////////////////////////////////////////////////////

CCfg:: CCfg(void) : m_dw_msec(CCfg::e_delay), m_wd_mode(TMode::e_info) { m_error << __MODULE__ << S_OK; }
CCfg::~CCfg(void) {}

/////////////////////////////////////////////////////////////////////////////

DWORD       CCfg::Delay (void) const       { return m_dw_msec; }
HRESULT     CCfg::Delay (const DWORD _val) {
	m_error << __MODULE__ << S_OK;
	if (0 == _val) return ((m_error << E_INVALIDARG) = _T("Delay value cannot be zero;"));
	else m_dw_msec = _val;
	return m_error;
}
TErrorRef   CCfg::Error (void) const       { return m_error;   }
const bool  CCfg::Is    (void) const       {
	m_error << __MODULE__ << S_OK;

	if (m_cs_path.IsEmpty()) (m_error << E_INVALIDARG) = _T("File path is not specified;");

	return (false == m_error);
}
const
TMode&      CCfg::Mode  (void) const       { return m_wd_mode; }
TMode&      CCfg::Mode  (void)             { return m_wd_mode; }
LPCTSTR     CCfg::Path  (void) const       { return (LPCTSTR)m_cs_path;}
HRESULT     CCfg::Path  (LPCTSTR _lp_sz_val) {
	m_error << __MODULE__ << S_OK;
	if (NULL == _lp_sz_val || 0 == ::_tcslen(_lp_sz_val))
		return ((m_error << E_INVALIDARG) = _T("Path argument is null or empty;"));
	m_cs_path = _lp_sz_val;
	return m_error;
}
HRESULT     CCfg::Set   (const CCommandLine& _cmd_ln) {
	m_error << __MODULE__ << S_OK;

	if (_cmd_ln.Has(_T("path")) == false) return ((m_error << E_INVALIDARG) = _T("Path argument is not specified;"));
	HRESULT hr_ = this->Path(_cmd_ln.Arg(_T("path")));
	if (FAILED(hr_))
		return hr_;

	if (_cmd_ln.Has(_T("delay")) == true) {
		const LONG l_val = _cmd_ln.Arg(_T("delay"), CCfg::e_delay);
		hr_ = this->Delay(l_val);
	}

	if (_cmd_ln.Has(_T("mode")) == true) {
		const LONG l_val = _cmd_ln.Arg(_T("mode"), TMode::e_info);
		hr_ = this->Mode() = CCfg_Params::IntToEnum(l_val);
	}

	return m_error;
}
/////////////////////////////////////////////////////////////////////////////

CCfg& CCfg::operator = (const CCommandLine& _cmd_ln) {
	this->Set(_cmd_ln);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CCfg& CCfg::GetObject(void) {
	static CCfg cfg_;
	return cfg_;
}