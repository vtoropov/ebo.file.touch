/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Jan-2016 at 5:58:51pm, GMT+7, Phuket, Rawai, Sunday;
	This is (thefileguardian.com) watch dog common definition(s) implementation file.
	-----------------------------------------------------------------------------
	Adopted to File Touch project on 6-Feb-2020 at 4:39:35p, UTC+7, Novosibirsk, Thursday;
*/
#include "StdAfx.h"
#include "file.wdog.com.defs.h"

using namespace file::touch::wdog;

/////////////////////////////////////////////////////////////////////////////

CStringW eSystemEvent::ToString(const _id _value) {
	CStringW cs_id(_T("no_event"));
	switch (_value) {
	case _id::eFileChanged: { cs_id = _T("modified"); } break;
	case _id::eFileCopied : { cs_id = _T("copied"  ); } break;
	case _id::eFileCreated: { cs_id = _T("created" ); } break;
	case _id::eFileDeleted: { cs_id = _T("deleted" ); } break;
	case _id::eFileRenamed: { cs_id = _T("renamed" ); } break;
	}
	return cs_id;
}

/////////////////////////////////////////////////////////////////////////////

CFile_base:: CFile_base(void) : m_event(TSysEvtId::eUndefined) { m_error << __MODULE__ << OLE_E_BLANK; }
CFile_base::~CFile_base(void) {}

/////////////////////////////////////////////////////////////////////////////

TSysEvtId   CFile_base::Compare(const CFile_base&  _rh_ref) const {

	TSysEvtId evt_id = TSysEvtId::eUndefined;

	if (this->Error() == false && _rh_ref.Error()) {
		if (this->Is(_rh_ref.Path())) (evt_id = TSysEvtId::eFileDeleted);
		else                          (evt_id = TSysEvtId::eFileRenamed);
	}
	if (this->Error() && _rh_ref.Error() == false) {
		if (_rh_ref.Is(this->Path())) (evt_id = TSysEvtId::eFileCreated);
		else                          (evt_id = TSysEvtId::eFileRenamed);
	}

	return evt_id;
}

TErrorRef   CFile_base::Error  (void) const  { return m_error; }
const
TSysEvtId&  CFile_base::Event  (void) const  { return m_event; }
TSysEvtId&  CFile_base::Event  (void)        { return m_event; }

const bool  CFile_base::Is     (LPCTSTR _lp_sz_path) const {
	if (NULL == _lp_sz_path || 0 == ::_tcslen(_lp_sz_path)) return false;
	return (0 == m_path.CompareNoCase(_lp_sz_path));
}
LPCTSTR     CFile_base::Path   (void) const  { return (LPCTSTR)m_path; }

/////////////////////////////////////////////////////////////////////////////
const
CDateTime&  CFile_base::Timestamp(void) const { return m_stamp; }
CDateTime&  CFile_base::Timestamp(void)       { return m_stamp; }

/////////////////////////////////////////////////////////////////////////////

CFile_base& CFile_base::operator= (const CFile_base& _ref) {
	this->m_error = _ref.m_error;
	this->m_path  = _ref.m_path ;
	this->m_event = _ref.m_event;
	this->m_stamp = _ref.m_stamp;

	return *this;
}

/////////////////////////////////////////////////////////////////////////////
#if (0)
CFile_compare:: CFile_compare(void) {}
CFile_compare::~CFile_compare(void) {}

/////////////////////////////////////////////////////////////////////////////

TSysEvtId   CFile_compare::Data(const CFileData& _lh_ref, const CFileData& _rh_ref) const {
	TSysEvtId evt_id = TSysEvtId::eUndefined;
	if (_lh_ref.Error() && _rh_ref.Error()) return evt_id;
	if (_lh_ref.Error() == false && _rh_ref.Error()) {
		if (_lh_ref.Is(_rh_ref.Path())) return (evt_id = TSysEvtId::eFileDeleted);
		else                            return (evt_id = TSysEvtId::eFileRenamed);
	}
	if (_lh_ref.Error() && _rh_ref.Error() == false) {
		if (_rh_ref.Is(_lh_ref.Path())) return (evt_id = TSysEvtId::eFileCreated);
		else                            return (evt_id = TSysEvtId::eFileRenamed);
	}
	if (_lh_ref.nFileSizeLow  != _rh_ref.nFileSizeLow ||
	    _lh_ref.nFileSizeHigh != _rh_ref.nFileSizeHigh)
		evt_id = TSysEvtId::eFileChanged;

	if (::CompareFileTime(&_lh_ref.ftLastWriteTime,&_rh_ref.ftLastWriteTime) != 0)
		evt_id = TSysEvtId::eFileChanged;

	return evt_id;
}

TSysEvtId   CFile_compare::Info(const CFileInfo& _lh_ref, const CFileInfo& _rh_ref) const {
	TSysEvtId evt_id = TSysEvtId::eUndefined;
	_lh_ref; _rh_ref;
	if (::CompareFileTime(&_lh_ref.ftCreationTime ,&_rh_ref.ftCreationTime ) != 0)
		evt_id = TSysEvtId::eFileCreated;
	if (::CompareFileTime(&_lh_ref.ftLastWriteTime,&_rh_ref.ftLastWriteTime) != 0) 
		evt_id = TSysEvtId::eFileChanged;

	return evt_id;
}
#endif