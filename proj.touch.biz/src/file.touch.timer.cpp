/*
	Created by Tech_dog (ebontrop@gmail.com) on 6-Feb-2020 at 11:07:50a, UTC+7, Novosibirsk, Thursday;
	This is File Touch project timer interface implementation file;
*/
#include "StdAfx.h"
#include "file.touch.timer.h"

using namespace file::touch::biz;
/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch { namespace biz { namespace _impl {
#if (1)
	DWORD WINAPI _thread_fun(CTimer* _p_tim){
		if (NULL == _p_tim)
			return ERROR_INVALID_ADDRESS;
		else
			return _p_tim->Thread_Fun();
	}
#endif
}}}}
using namespace file::touch::biz::_impl;
/////////////////////////////////////////////////////////////////////////////

CTimer:: CTimer(ITimerEventSink* _p_snk) : m_p_sink(_p_snk), m_thread(NULL), m_stop_evt(NULL),
	m_adapter(*this, _variant_t((LONG)1)), m_break_it_on(false), m_delay(100, 500) {
	m_error << __MODULE__ << S_OK;
}
CTimer::~CTimer(void) {}

/////////////////////////////////////////////////////////////////////////////
const
CDelayEvent& CTimer::Delay(void) const { return m_delay; }
CDelayEvent& CTimer::Delay(void)       { return m_delay; }

TErrorRef    CTimer::Error(void) const { return m_error; }
bool         CTimer::IsOn (void) const { return NULL != m_thread; }
VOID         CTimer::Sink (ITimerEventSink* _p_snk) { this->m_p_sink = _p_snk; }
HRESULT      CTimer::Turn (const bool _b_on ) {
	m_error << __MODULE__ << S_OK;
	if (_b_on == true  && this->IsOn() == true ) return (m_error = __DwordToHresult(ERROR_INVALID_STATE));
	if (_b_on == false && this->IsOn() == false) return (m_error = __DwordToHresult(ERROR_INVALID_STATE));
	if (_b_on) {

		m_break_it_on = false;
		DWORD dw_id_  = 0;

		if (TCfg::GetObject().Is() == false)
			return (m_error = TCfg::GetObject().Error());
		
		HRESULT hr_ = m_adapter.Create();
		if (FAILED(hr_))
			return (m_error = hr_);
		m_stop_evt  = ::CreateEvent(NULL, TRUE, FALSE, NULL);

		m_thread = ::CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)_thread_fun, this, 0, &dw_id_);
		if (NULL == m_thread)
			hr_  = __DwordToHresult(::GetLastError());
	}
	else {
		m_break_it_on = true;

		if (m_stop_evt) { ::WaitForSingleObject(m_stop_evt, INFINITE);  }
		if (m_stop_evt) { ::CloseHandle(m_stop_evt);  m_stop_evt = NULL;}
		if (m_thread)   { ::CloseHandle(m_thread)  ;  m_thread = NULL;  }

		m_adapter.Destroy();
	}
	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT  CTimer::GenericEvent_OnNotify(const _variant_t v_evt_id) {
	v_evt_id;

	if (m_error && m_p_sink) m_p_sink->ITimer_OnError(m_error);

	return S_OK;
}

/////////////////////////////////////////////////////////////////////////////

DWORD    CTimer::Thread_Fun (void) { HRESULT hr_ = S_OK;

	CFileData the_data;
	CFileInfo the_info;

	const bool  b_use_data = (TMode::e_data == TCfg::GetObject().Mode());
	if (true == b_use_data) {
		the_data.Set(TCfg::GetObject().Path());
		if   (m_p_sink && the_data.Error()) m_p_sink->ITimer_OnError(the_data.Error());
		else if (m_p_sink)                  m_p_sink->ITimer_OnEvent(the_data);
	}
	else {
		the_info.Set(TCfg::GetObject().Path());
		if    (m_p_sink && the_info.Error()) m_p_sink->ITimer_OnError(the_info.Error());
		else if (m_p_sink)                   m_p_sink->ITimer_OnEvent(the_info);
	}

	while (m_break_it_on == false) {

		m_delay.Wait();
		if (m_delay.Elapsed()) m_delay.Reset();
		else
			continue;

		if (TCfg::GetObject().Is() == false) // not necessary; no configuration is acceptible in this version;
			continue;

		if (true == b_use_data) {
			CFileData dyn_data;
			hr_ = dyn_data.Set(TCfg::GetObject().Path());
			if (dyn_data.Error()) {
				if (the_data.Error() != dyn_data.Error()) {
					the_data = dyn_data;
					if (m_p_sink) m_p_sink->ITimer_OnError(dyn_data.Error());
				} continue  ;
			}
			const TSysEvtId evt_id = the_data.Compare(dyn_data);
			if (TSysEvtId::eUndefined != evt_id) {
				dyn_data.Event() = evt_id;
				the_data  = dyn_data;
				if (m_p_sink) m_p_sink->ITimer_OnEvent(the_data);
			}
		}
		else {
			CFileInfo dyn_info;
			hr_ = dyn_info.Set(TCfg::GetObject().Path());
			if (dyn_info.Error()) {
				if (the_info.Error() != dyn_info.Error()) {
					the_info = dyn_info;
					if (m_p_sink) m_p_sink->ITimer_OnError(dyn_info.Error());
				} continue  ;
			}
			const TSysEvtId evt_id = the_info.Compare(dyn_info);
			if (TSysEvtId::eUndefined != evt_id) {
				dyn_info.Event() = evt_id;
				the_info  = dyn_info;
				if (m_p_sink) m_p_sink->ITimer_OnEvent(the_info);
			}
		}
	}
	if (m_stop_evt) ::SetEvent(m_stop_evt);
	return NO_ERROR;
}

/////////////////////////////////////////////////////////////////////////////

TTimer& TTimer::GetObject(void) { static TTimer timer; return timer; }