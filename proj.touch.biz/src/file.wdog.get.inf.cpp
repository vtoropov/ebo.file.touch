/*
	Created by Tech_dog (ebontrop@gmail.com) on 9-Feb-2020 at 9:22:10p, UTC+7, Novosibirsk, Sunday;
	This is File Touch project watch dog getting file information interface implementation file;
*/
#include "StdAfx.h"
#include "file.wdog.get.inf.h"

using namespace file::touch::wdog;

#include "shared.gen.han.h"
#include "shared.gen.sys.time.h"

using namespace shared::lite::common;
/////////////////////////////////////////////////////////////////////////////

CFileInfo::CFileInfo(void) : TBase() { ::memset((TFileInfo*)this, 0, sizeof(TFileInfo)); }
CFileInfo::CFileInfo(const CFileInfo& _ref) : TBase() { *this = _ref; }

/////////////////////////////////////////////////////////////////////////////

TSysEvtId    CFileInfo::Compare(const CFileInfo&  _rh_ref) const {

	TSysEvtId evt_id = TBase::Compare(_rh_ref);

	if (evt_id != TSysEvtId::eUndefined)
		return evt_id;
	if (this->Error() && _rh_ref.Error())
		return evt_id;

	if (::CompareFileTime(&this->ftCreationTime ,&_rh_ref.ftCreationTime ) != 0)
		return (evt_id = TSysEvtId::eFileCreated);
	if (::CompareFileTime(&this->ftLastWriteTime,&_rh_ref.ftLastWriteTime) != 0) 
		return (evt_id = TSysEvtId::eFileChanged);

	if (this->nFileSizeLow  != _rh_ref.nFileSizeLow ||
		this->nFileSizeHigh != _rh_ref.nFileSizeHigh)
		evt_id = TSysEvtId::eFileChanged;

	return evt_id;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT     CFileInfo::Set    (LPCTSTR _lp_sz_path) {
	m_error << __MODULE__ << S_OK;

	m_stamp.Refresh();

	if (NULL == _lp_sz_path || 0 == ::_tcslen(_lp_sz_path))
		return ((m_error = E_INVALIDARG) = _T("File path cannot be empty;"));
	else
		m_path = _lp_sz_path;

	CAutoHandle hFile = ::CreateFile(
		_lp_sz_path    ,
		GENERIC_READ   ,
		FILE_SHARE_READ|FILE_SHARE_WRITE,
		NULL           ,
		OPEN_EXISTING  ,
		FILE_FLAG_SEQUENTIAL_SCAN,
		0
	);
	if (hFile.IsValid() == false) {
		m_error = ::GetLastError();
		return m_error;
	}

	if (::GetFileInformationByHandle(hFile, this) == false) {
		m_error = ::GetLastError();
		return m_error;
	}

	return m_error;
}

/////////////////////////////////////////////////////////////////////////////

CFileInfo& CFileInfo::operator= (const CFileInfo& _ref) {
	(CFile_base&)*this = (CFile_base&)_ref;

	const errno_t err_ = ::memcpy_s((TFileInfo*)this, sizeof(TFileInfo), (TFileInfo*)&_ref, sizeof(TFileInfo));
	if (0 != err_) {
		(this->m_error = E_OUTOFMEMORY) = _T("Copying data is failed;");
	}
	return *this;
}

CFileInfo& CFileInfo::operator<<(LPCTSTR _lp_sz_path) {
	this->Set(_lp_sz_path);
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch { namespace wdog {

bool operator==(const CFileInfo& _lhs, const CFileInfo& _rhs) {
	return TSysEvtId::eUndefined == _lhs.Compare(_rhs);
}
bool operator!=(const CFileInfo& _lhs, const CFileInfo& _rhs) {
	return TSysEvtId::eUndefined != _lhs.Compare(_rhs);
}

}}}