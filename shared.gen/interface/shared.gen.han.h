#ifndef _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
#define _SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 29-Nov-2014 at 0:42:43am, GMT+3, Taganrog, Saturday;
	This is Shared Lite Library Generic Handle class declaration file.
	-----------------------------------------------------------------------------
	Adopted to File Touch project on 6-Feb-2020 at 8:57:50p, UTC+7, Novosibirsk, Thursday;
*/
#include "shared.gen.sys.err.h"

namespace shared { namespace lite { namespace common
{
	using shared::sys_core::CError;

	class CAutoHandle
	{
	private:
		HANDLE     m_handle;
	public:
		 CAutoHandle(void);
		 CAutoHandle(const HANDLE);
		~CAutoHandle(void);
	public:
		CAutoHandle& operator=(const HANDLE);
	public:
		operator   HANDLE(void);
		operator   HANDLE(void) const;
	public:
		PHANDLE    operator&(void);
	private:
		CAutoHandle(const CAutoHandle&);
		CAutoHandle& operator= (const CAutoHandle&);
	public:
		HANDLE     Handle(void) const;
		bool       IsValid(void) const;
		void       Reset(void);
	};

	class CAutoHandleArray
	{
	private:
		PHANDLE    m_handles;
		DWORD      m_size;
		CError     m_error;
	public:
		 CAutoHandleArray(const DWORD dwSize);
		~CAutoHandleArray(void);
	public:
		TErrorRef  Error(void)const;
		bool       IsValid(void)const;
		PHANDLE    Handles(void)const;
		DWORD      Size(void)const;
	public:
		HANDLE     operator[] (const INT) const;
		HANDLE&    operator[] (const INT)      ;
	private:
		CAutoHandleArray(const CAutoHandleArray&);
		CAutoHandleArray& operator= (const CAutoHandleArray&);
	};
}}}

#endif/*_SHAREDLITEGENERICHANDLE_H_212F82EC_7E06_468a_8DEA_B2A98F4F2D6F_INCLUDED*/