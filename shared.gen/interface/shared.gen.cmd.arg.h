#ifndef _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
#define _SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2019 at 6:52:05a, UTC+7, Novosibirsk, Sunday;
	This is FIX Engine shared library configuration command line interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to File Touch app on 2-Feb-2020 at 8:15:18p, UTC+7, Novosibirsk, Sunday;
*/

namespace shared { namespace common {

	typedef ::std::map<::ATL::CStringW, ::ATL::CStringW> TCmdLineArgs;

	class CCommandLine {
	private:
		CStringW   m_module_full_path;
		TCmdLineArgs m_args;
	public:
		 CCommandLine(void);
		~CCommandLine(void);
	public:
		HRESULT      Append(LPCTSTR _lp_sz_nm, LPCTSTR _lp_sz_val);
		CStringW   Arg   (LPCTSTR _lp_sz_nm) const;
		LONG         Arg   (LPCTSTR _lp_sz_nm, const LONG _def_val)const;
		TCmdLineArgs Args  (void)const;     // returns a copy of command line argument collection
		VOID         Clear (void)     ;
		INT          Count (void)const;
		bool         Has   (LPCTSTR pszArgName)const;
		CStringW   ModuleFullPath(void)const;
		CStringW   ToString (LPCTSTR _lp_sz_sep = NULL) const;
	};
}}

#endif/*_SHAREDGENCMDLN_H_AF7E605A_D692_4F3E_A601_DD8DB84A6516_INCLUDED*/