/*
	Created by Tech_dog (ebontrop@gmail.com) on 10-Feb-2019 at 6:52:05a, UTC+7, Novosibirsk, Sunday;
	This is FIX Engine shared library configuration command line interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to sound-bin-trans project on 5-Apr-2019 at 11:55:19a, UTC+7, Phuket, Rawai, Thursday;
	Adopted to serial port data project on 21-May-2019 at 7:16:44p, UTC+7, Phuket, Rawai, Tuesday;
	Adopted to File Touch app on 2-Feb-2020 at 8:15:18p, UTC+7, Novosibirsk, Sunday;
*/
#include "StdAfx.h"
#include "shared.gen.cmd.arg.h"

using namespace shared::common;

#include <shellapi.h>

/////////////////////////////////////////////////////////////////////////////

CCommandLine:: CCommandLine(void)
{
	::ATL::CStringW cs_cmd_line = ::GetCommandLine();
	::ATL::CStringW cs_key;
	::ATL::CStringW cs_arg;

	INT n_count = 0;
	bool bKey   = false;

	LPWSTR* pCmdArgs = ::CommandLineToArgvW(cs_cmd_line.GetString(), &n_count);
	if (0 == n_count || NULL == pCmdArgs)
		goto __end_of_story__;
	
	m_module_full_path = pCmdArgs[0];

	for (INT i_ = 1; i_ < n_count; i_+= 1) {
		::ATL::CStringW cs_val = pCmdArgs[i_];
		bKey = (/*0 == cs_val.Find(_T("-")) // negative numbers are eaten when minus appears, quotes around value must be considered;
				||*/ 0 == cs_val.Find(_T("/")));

		if (bKey) {
			if (cs_key.IsEmpty() == false) // the previous key is not saved yet;
				m_args.insert(::std::make_pair(cs_key, cs_arg));

			cs_key = pCmdArgs[i_]; cs_key.Replace(_T("-"), _T("")); cs_key.Replace(_T("/"), _T(""));
			cs_arg = _T("");
		}
		else {
			cs_arg+= pCmdArgs[i_];
		}

		const bool bLast = (i_ == n_count - 1);
		if (bLast && cs_key.IsEmpty() == false) {
			try {
				m_args.insert(::std::make_pair(cs_key, cs_arg));
			} catch (::std::bad_alloc&){}
		}
	}

__end_of_story__:
	if (NULL != pCmdArgs) {
		::LocalFree(pCmdArgs); pCmdArgs = NULL;
	}
}

CCommandLine::~CCommandLine(void) { this->Clear(); }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CCommandLine::Append(LPCTSTR _lp_sz_nm, LPCTSTR _lp_sz_val) {
	HRESULT hr_ = S_OK;
	try {
		m_args.insert(::std::make_pair(
			CStringW(_lp_sz_nm), CStringW(_lp_sz_val)
		));
	}
	catch(::std::bad_alloc&){
		hr_ = E_OUTOFMEMORY;
	}
	return hr_;
}

CStringW    CCommandLine::Arg   (LPCTSTR _lp_sz_nm)const
{
	TCmdLineArgs::const_iterator it_ = m_args.find(::ATL::CStringW(_lp_sz_nm));
	if (it_ == m_args.end())
		return ::ATL::CStringW();
	else
		return it_->second;
}

LONG          CCommandLine::Arg   (LPCTSTR _lp_sz_nm, const LONG _def_val)const
{
	TCmdLineArgs::const_iterator it_ = m_args.find(::ATL::CStringW(_lp_sz_nm));
	if (it_ == m_args.end())
		return _def_val;
	else
		return ::_tstol(it_->second);
}

TCmdLineArgs  CCommandLine::Args  (void) const { return m_args; }
VOID          CCommandLine::Clear (void)       { if (m_args.empty() == false) m_args.clear(); }
INT           CCommandLine::Count (void)const  { return static_cast<INT>(m_args.size()); }
bool          CCommandLine::Has   (LPCTSTR pArgName) const
{
	TCmdLineArgs::const_iterator it__ = m_args.find(::ATL::CStringW(pArgName));
	return (it__ != m_args.end());
}

CStringW    CCommandLine::ModuleFullPath(void) const { return m_module_full_path; }

CStringW    CCommandLine::ToString(LPCTSTR _lp_sz_sep) const
{
	LPCTSTR lp_sz_pat = _T("%s=%s");
	CStringW cs_pat ;
	CStringW cs_args;
	if (m_args.empty())
		return (cs_args = _T("command line has no argument;"));

	for (TCmdLineArgs::const_iterator it_ = m_args.begin(); it_ != m_args.end(); ++it_)
	{
		cs_pat.Format(
				lp_sz_pat, (LPCTSTR)it_->first, (LPCTSTR)it_->second
			);
		cs_args += cs_pat;
		cs_args +=(NULL == _lp_sz_sep ? _T("; ") : _lp_sz_sep);
	}
	return cs_args;
}