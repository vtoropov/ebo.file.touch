/*
	Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 on 10:23:21p, UTC+7, Phuket, Rawai, Thursday;
	This is sound-bin-trans receiver desktop console application entry point file.
	-----------------------------------------------------------------------------
	Adopted to FakeGPS project on 13-Dec-2019 at 12:11:50p, UTC+7, Novosibirsk, Friday;
	Adopted to File Touch App project on 02-Feb-2020 at 7:54:18p, UTC+7, Novosibirsk, Sunday;
*/
#include "StdAfx.h"
#include "shared.gen.sys.com.h"
#include "shared.gen.sys.err.h"

using namespace shared::sys_core;

#include "shared.gen.cmd.arg.h"
#include "shared.gen.con.bas.h"

using namespace shared::common;
using namespace shared::common::ui;

#include "file.touch.res.h"
#include "file.touch.cfg.h"

#include "file.touch.timer.h"

/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch { namespace _impl {

	using file::touch::wdog::CFileData;
	using file::touch::wdog::CFileInfo;
	using file::touch::wdog::TSysEvtId;
	using file::touch::wdog::eSystemEvent;

	static LPCTSTR lp_sz_evt_pat = _T("Event info:\n\t\tpath: %s;\n\t\tstatus=%s;");

	class CConsole_Rcv : public shared::common::ui::CConsole, public file::touch::biz::ITimerEventSink {
	                    typedef shared::common::ui::CConsole  TBase;
	private:
		CError        m_error;
		CFileData     m_data ;
		CFileInfo     m_info ;
	public:
		CConsole_Rcv(void) {
			m_error << __MODULE__ << S_OK;
			CStringW cs_title(_T("File Touch Project Console"));
#if defined(WIN64)
			cs_title += _T(" [x64]");
#else
			cs_title += _T(" [x86]");
#endif
			TBase::OnCreate(cs_title.GetString());
			TBase::SetIcon (IDR_FILE_TOUCH_ICO);
		}
	private: // ITimerEventSink
#pragma warning (disable: 4481)
		virtual HRESULT ITimer_OnError(const CError& _err) override sealed {

			*this << _err;
			
			HRESULT hr_ = S_OK;
			return  hr_;
		}
		virtual HRESULT ITimer_OnEvent(const CFileData& _data) override sealed {

			m_data  = _data;

			CStringW cs_msg; cs_msg.Format(
				lp_sz_evt_pat, m_data.Path(), (LPCTSTR)eSystemEvent::ToString(m_data.Event())
			);

			*this << cs_msg;
			HRESULT hr_ = S_OK;
			return  hr_;
		}
		virtual HRESULT ITimer_OnEvent(const CFileInfo& _info) override sealed {

			m_info  = _info;
			
			CStringW cs_msg; cs_msg.Format(
				lp_sz_evt_pat, m_info.Path(), (LPCTSTR)eSystemEvent::ToString(m_info.Event())
			);

			*this << cs_msg;
			HRESULT hr_ = S_OK;
			return  hr_;
		}
#pragma warning (default: 4481)
	};
}}}
using namespace file::touch::_impl;
using namespace file::touch::biz;
/////////////////////////////////////////////////////////////////////////////

INT _tmain(VOID) {
	INT n_res = 0;

	::SetConsoleOutputCP(CP_UTF8);

	CConsole_Rcv con_;

	CCoIniter com_lib(false);
	if (com_lib == false) {
		con_.WriteErr(com_lib.Error());
		return (n_res = 1);
	}

#pragma region _sec
	CCoSecurityProvider sec_;
	HRESULT hr_ = sec_.InitNoIdentity();
	if (FAILED(hr_)) {
		con_.WriteErr(sec_.Error());
		return (n_res = 1);
	}
#pragma endregion

	CError sys_err; sys_err << __MODULE__ << S_OK;
	CCommandLine cmd_line;

	TCfg& cfg_ = TCfg::GetObject(); //#pragma warning (disable: 4481)

	cfg_ = cmd_line;
	if (cfg_.Error()) {
		con_ << cfg_.Error();
	}
	else {
		static LPCTSTR lp_sz_pat = _T("%spath = %s%sdelay= %d(msec)%smode = %s");
		CStringW cs_text;
		cs_text.Format(
			lp_sz_pat, 
			lp_sz_sep, cfg_.Path(),
			lp_sz_sep, cfg_.Delay(),
			lp_sz_sep, (LPCTSTR)TParams::EnumToText(cfg_.Mode())
		);
		con_ << cs_text;
	}

	TTimer::GetObject().Sink(&con_);
	TTimer::GetObject().Delay().Reset(100, cfg_.Delay());

	hr_ =  TTimer::GetObject().Turn(true);
	if (FAILED(hr_)) {
		con_ << TTimer::GetObject().Error();
	}
	
	if (SUCCEEDED(hr_)) {
		con_.OnWait(_T("Press any key to stop timer;\n"));
		hr_ =  TTimer::GetObject().Turn(false);
	}
	con_.OnWait(_T("Press any key or click [x] window button to exit.\n"));
	return n_res;
}