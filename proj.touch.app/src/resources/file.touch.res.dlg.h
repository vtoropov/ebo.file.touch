//
// Created by Tech_dog (ebontrop@gmail.com) on 3-Dec-2018 at 6:50:00a, UTC+7, Novosibirsk, Tulenina, Monday;
// This is USB Drive Detective (http://www.bitsphereinc.com/) desktop app data wizard UI control identifier declaration file.
// -----------------------------------------------------------------------------
// Adopted to Ebo Pack Google push notification app on 14-Sep-2019 at 3:46:28p, UTC+7, Novosibirsk, Tulenina, Saturday;
//

#pragma region __main_dlg

#define IDD_FILE_TOUCH_DLG_MAIN          1101
#define IDC_FILE_TOUCH_DLG_MAIN_BAN      1103
#define IDR_FILE_TOUCH_DLG_MAIN_BAN      IDC_FILE_TOUCH_DLG_MAIN_BAN
#define IDC_FILE_TOUCH_DLG_MAIN_TAB      1105

#pragma endregion

#pragma region __main_page_state

#define IDD_FILE_TOUCH_PAG_STATE         1201
#define IDC_FILE_TOUCH_PATH              1203
#define IDC_FILE_TOUCH_MODE              1205
#define IDC_FILE_TOUCH_STATE             1207
#define IDC_FILE_TOUCH_REFRH             1209

#pragma endregion

