//
//   Created by Tech_dog (ebontrop@gmail.com) on 5-Apr-2019 at 10:18:17p, UTC+7, Phuket, Rawai, Thursday;
//   This is sound-bin-trans receiver desktop console application resource declaration file.
//   -----------------------------------------------------------------------------
//   Adopted to FakeGPS project on 13-Dec-2019 at 8:28:09a, UTC+7, Novosibirsk, Friday;
//   Adopted to File Touch project on 02-Feb-2020 at 1:43:15p, UTC+7, Novosibirsk, Sunday;
//

#pragma region __generic
#define IDR_FILE_TOUCH_ICO           1001
#pragma endregion

#include "file.touch.res.dlg.h"

#pragma region __sys_tray_shortcut

#define IDM_FILE_TOUCH_TRAY_SHORTCUT 1501
#define IDC_FILE_TOUCH_TRAY_EXIT     1503
#define IDC_FILE_TOUCH_TRAY_GUI_SHOW 1505

#pragma endregion
