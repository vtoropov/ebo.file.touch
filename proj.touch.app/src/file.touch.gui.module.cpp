/*
	Created by Tech_dog (ebontrop@gmail.com) on 23-Nov-2015 at 6:38:36pm, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring antivirus app entry point implementation file.
	-----------------------------------------------------------------------------
	Adopted to File Touch app on 11-Feb-2020 at 7:06:02p, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "shared.gen.cmd.arg.h"
#include "shared.gui.gdi.plus.h"

using namespace shared::gui::draw;

#include "shared.gen.sys.com.h"

using namespace shared::sys_core;

WTL::CAppModule _Module;

#include "file.touch.cfg.h"
#include "file.touch.dlg.main.h"
#include "file.touch.tray.hand.h"
#include "file.touch.res.h"

using namespace file::touch;
using namespace file::touch::biz;
/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch {

	INT Run(VOID) {

		HRESULT hr_ = S_OK;

		file::touch::biz::CCfg::GetObject() = shared::common::CCommandLine();
		file::touch::gui::CMainDlg man_dlg;

		file::touch::CSystemTray sys_tray(man_dlg);
		if (sys_tray.Error()) {
			sys_tray.Error().Show(); return (1); // an error occurred during notification area initialization;
		}
		
		sys_tray.Area().Icon().Show(
			IDR_FILE_TOUCH_ICO, _T("Starting File Touch...")
		);
		if (sys_tray.Area().Icon().Error()) {
			sys_tray.Area().Icon().Error().Show(); return (1);
		}

		INT n_res_ = 0;
		CCfg& cfg_ = file::touch::biz::CCfg::GetObject();

		TTimer::GetObject().Sink(&man_dlg.GetSink());
		TTimer::GetObject().Delay().Reset(100, cfg_.Delay());
		{
			if (SUCCEEDED(hr_)) {

				hr_ =  TTimer::GetObject().Turn(true);
				if (SUCCEEDED(hr_)) {
				}
				else
					TTimer::GetObject().Error().Show();

				::WTL::CMessageLoop pump_;

				_Module.AddMessageLoop(&pump_);
				n_res_ = pump_.Run();
				_Module.RemoveMessageLoop();

				hr_ =  TTimer::GetObject().Turn(false);
			}
		}
		sys_tray.Area().Icon().Hide();
		return n_res_;
	}

}}

INT WINAPI _tWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPTSTR lpstrCmdLine, INT nCmdShow)
{
	hInstance; hPrevInstance; lpstrCmdLine; nCmdShow;

	CCoIniter com_lib(false);
	if (com_lib.Error()) {
		com_lib.Error().Show();
		return 1;
	}
	CCoSecurityProvider sec_prov;
	HRESULT hr_ = sec_prov.InitNoIdentity();

	if (FAILED(hr_))
		sec_prov.Error().Show();
	/*
		Tech_dog commented on 09-Feb-2010 at 12:47:50pm, UTC+3, Rostov-on-Don, Tuesday:
		_____________________________________________________________________________
		we need to assign library identifier for linking ATL DLL statically,
		otherwise we get annoying fkn message "Did you forget to pass the LIBID to CComModule::Init?"
	*/
	_Module.m_libid = LIBID_ATLLib;

	INT nResult = 0;
	// this resolves ATL window thunking problem when Microsoft Layer for Unicode (MSLU) is used;
	::DefWindowProc(NULL, 0, 0, 0L);

	AtlInitCommonControls(ICC_BAR_CLASSES);	// add flags to support other controls

	hr_ = _Module.Init(NULL, hInstance);
	if(!SUCCEEDED(hr_))
	{
		ATLASSERT(FALSE);
		return 1;
	}

	CGdiPlusLib_Guard  gdi_guard;
	if (gdi_guard.Secured()==false)
		gdi_guard.Capture();

	nResult = file::touch::Run();

	_Module.Term();
	
	return nResult;
}