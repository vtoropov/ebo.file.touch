#ifndef _UDDGENDLGSETVIEW_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
#define _UDDGENDLGSETVIEW_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:38:45a, UTC+7, Novosibirsk, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 1:26:03p, UTC+7, Novosibirsk, Sunday;
	Adopted to File Touch project desktop app on 12-Feb-2020 at 9:17:39p, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gen.syn.obj.h"
#include "shared.gui.page.base.h"
#include "file.touch.timer.h"

namespace file { namespace touch { namespace gui {

	using shared::sys_core::CError;
	using shared::sys_core::CSyncObject;

	using shared::gui::ITabPageEvents ;
	using shared::gui::ITabSetCallback;
	using shared::gui::CTabPageBase   ;

	using file::touch::biz::ITimerEventSink;

	using file::touch::wdog::CFileInfo;
	using file::touch::wdog::CFileData;

	using file::touch::wdog::TSysEvtId;
	using file::touch::wdog::eSystemEvent;

	class CPageState_Out {
	private:
		CWindow     m_page ;
		CError      m_error; // the last timer error;
		CFileData   m_data ; // the last file  data ;
		CFileInfo   m_info ; // the last file  info ;
		CSyncObject m_guard; // for thread-safe: timer procedure is running separatly in work thread;

	public:
		 CPageState_Out(void);
		~CPageState_Out(void);

	public:
		VOID  Do(void);
		CSyncObject& Lock(void);

	public:
		CPageState_Out& operator << (const HWND _h_page);
		CPageState_Out& operator << (TErrorRef) ;
		CPageState_Out& operator << (const CFileData&)  ;
		CPageState_Out& operator << (const CFileInfo&)  ;
	};

	class CPageState : public CTabPageBase, public  ITabPageEvents, public ITimerEventSink {
	                  typedef CTabPageBase  TPage;
	private:
		CPageState_Out m_output;

	public :
		 CPageState(ITabSetCallback&);
		~CPageState(void);

	private:
#pragma warning(disable: 4481)
		virtual LRESULT     OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
		virtual LRESULT     OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;

	private: // ITabPageCallback
		virtual LRESULT     TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) override sealed;
	private: // ITimerEventSink
		virtual HRESULT     ITimer_OnError(const CError&   ) override sealed;
		virtual HRESULT     ITimer_OnEvent(const CFileData&) override sealed;
		virtual HRESULT     ITimer_OnEvent(const CFileInfo&) override sealed;

	public : // TBasePage
		virtual bool        IsChanged   (void) const override sealed;
		virtual CStringW    GetPageTitle(void) const override sealed;
		virtual VOID        UpdateData  (const DWORD _opt = 0) override sealed; // temporarily disabled due to possible invocation after control(s) destroying;
#pragma warning(default: 4481)
	public:
	};

}}}

#endif/*_UDDGENDLGSETVIEW_H_38DF0120_8B20_4148_A304_50CDF477031A_INCLUDED*/