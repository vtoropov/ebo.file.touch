/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Sep-2018 at 11:44:40a, UTC+7, Novosibirsk, Sunday;
	This is USB Drive Detective (bitsphereinc.com) main view options dialog page interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to Ebo Pack Google push notification app on 15-Sep-2019 at 2:43:52p, UTC+7, Novosibirsk, Sunday;
	Adopted to File Touch project desktop app on 12-Feb-2020 at 9:32:43a, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "file.touch.pag.state.h"
#include "file.touch.res.h"
#include "file.touch.cfg.h"

using namespace file::touch::gui;
using namespace file::touch::biz;

#include "shared.gui.gdi.plus.h"
#include "shared.gui.page.layout.h"

using namespace shared::gui::draw;
using namespace shared::gui::layout;

#include "shared.gen.sys.time.h"

using namespace shared::lite::common;

/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch { namespace gui { namespace details
{
	class CPageState_Ctl {
	public:
		enum _ctl : WORD {
			e_ctl_path = IDC_FILE_TOUCH_PATH ,    // file path edit box;
			e_ctl_mode = IDC_FILE_TOUCH_MODE ,    // getting info mode combo box;
			e_ctl_stat = IDC_FILE_TOUCH_STATE,    // output multiline edit box;
			e_btn_refs = IDC_FILE_TOUCH_REFRH,    // refresh button;
		};
	};
	typedef CPageState_Ctl This_Ctl;

	class CPageState_Layout
	{
	private:
		const CWindow&   m_page_ref;
		ITabSetCallback& m_page_snk;
		RECT             m_page_rec;

	public:
		CPageState_Layout(const CWindow& page_ref, ITabSetCallback& _snk) : m_page_ref(page_ref), m_page_snk(_snk) {
			if (m_page_ref.IsWindow())
				m_page_ref.GetClientRect(&m_page_rec);
			else
				::SetRectEmpty(&m_page_rec);
		}

	public:
		VOID   OnCreate(void) {
			const RECT rc_path = Lay_(m_page_ref) = This_Ctl::e_ctl_path;
			CPage_Layout(m_page_ref).AlignComboHeightTo(This_Ctl::e_ctl_mode, __H(rc_path));

		}
		VOID   OnSize  (void) {
		}
	};

	class CPageState_Init
	{
	private:
		CWindow& m_page_ref;
		
	public:
		CPageState_Init(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
		}

	public:
		VOID   OnCreate  (void) {

			CWindow w_path = Lay_(m_page_ref) << This_Ctl::e_ctl_path;
			if (0!= w_path) {
				w_path.SetWindowText(CCfg::GetObject().Path());
			}

			WTL::CComboBox  cbo_mode = Lay_(m_page_ref) << This_Ctl::e_ctl_mode;
			if (cbo_mode) {
				cbo_mode.AddString(_T("Retrieving File Info by Handle"));
				cbo_mode.AddString(_T("Searching File by Path"));

				cbo_mode.SetCurSel(
					file::touch::biz::CCfg::GetObject().Mode()
				);
			}

		}
		VOID   UpdateData(void) {}
	};

	class CPageState_Handler
	{
	private:
		CWindow& m_page_ref;

	public:
		CPageState_Handler(CWindow& dlg_ref) : m_page_ref(dlg_ref) {}

	public:
		BOOL   OnCommand (const WORD ctrlId, const WORD wNotify) {
			wNotify; ctrlId;
			BOOL   bHandled = FALSE;
			if (This_Ctl::e_btn_refs == ctrlId) {

				if (TTimer::GetObject().IsOn()) TTimer::GetObject().Turn(false);

				CWindow w_path = Lay_(m_page_ref) << This_Ctl::e_ctl_path;
				if (0!= w_path) {
					CStringW cs_path;
					w_path.GetWindowTextW(cs_path);

					TCfg::GetObject().Path(cs_path.GetString());
				}

				WTL::CComboBox  cbo_mode = Lay_(m_page_ref) << This_Ctl::e_ctl_mode;
				if (cbo_mode) {
					const INT n_selected = cbo_mode.GetCurSel();
					if (0 == n_selected) TCfg::GetObject().Mode() = TMode::e_info;
					else TCfg::GetObject().Mode() = TMode::e_data;
				}

				TTimer::GetObject().Turn(true);

				bHandled = TRUE;
			}
			return bHandled;
		}
	};

	class CPageState_Fmt{
	private:
		CWindow& m_page_ref;

	public:
		CPageState_Fmt(CWindow& dlg_ref) : m_page_ref(dlg_ref) {
		}

	public:
		VOID    Format(const CFileData& _dat) {

			CStringW cs_sta = eSystemEvent::ToString(_dat.Event());
			CStringW cs_out ; cs_out.Format (
				this->GetEvtPat(),
				(LPCTSTR)_dat.Timestamp().GetTimeAsUnix(), _dat.Path(), (LPCTSTR)cs_sta
			);
			this->Output((LPCTSTR)cs_out);
		}

		VOID    Format(const CFileInfo& _inf) {

			CStringW cs_sta = eSystemEvent::ToString(_inf.Event());
			CStringW cs_out ; cs_out.Format (
				this->GetEvtPat(),
				(LPCTSTR)_inf.Timestamp().GetTimeAsUnix(), _inf.Path(), (LPCTSTR)cs_sta
			);
			this->Output((LPCTSTR)cs_out);
		}

		VOID    Format(TErrorRef _err) {
			CStringW cs_out ; cs_out.Format (
				this->GetErrPat(),
				(LPCTSTR)this->GetTimestamp() , _err.Result(), _err.Desc(), _err.Module()
			);
			this->Output((LPCTSTR)cs_out);
		}

		VOID    Refresh(void) {
			WTL::CEdit ctl_out = Lay_(m_page_ref) << This_Ctl::e_ctl_stat;
			if (ctl_out) {
				CStringW& cs_out = this->_get_int_buffer();
				ctl_out.SetWindowText((LPCTSTR)cs_out);

				this->UpdateScroll();
			}
		}

	private:
		LPCTSTR GetErrPat(void) const {
			static LPCTSTR lp_sz_pat = _T(
				"[%s]\r\n"
				"Event Error:\r\n"
				"  code\t= 0x%x;\r\n"
				"  desc\t= %s;\r\n"
				"  module\t= %s;");
			return lp_sz_pat;
		}

		LPCTSTR GetEvtPat(void) const {
			static LPCTSTR lp_sz_pat = _T("[%s]\r\nEvent Info:\r\n\tpath: %s;\r\n\tstatus=%s;");
			return lp_sz_pat;
		}

		CStringW GetTimestamp(void) const {
			CDateTime current_;
			return current_.GetTimeAsUnix();
		}

		VOID    Output(LPCTSTR _lp_sz_msg) {
			if (NULL == _lp_sz_msg || 0 == ::_tcslen(_lp_sz_msg)) return;
			// https://docs.microsoft.com/en-us/windows/win32/controls/em-linescroll
			WTL::CEdit ctl_out = Lay_(m_page_ref) << This_Ctl::e_ctl_stat;
			if (ctl_out) {
				CStringW& cs_out = this->_get_int_buffer();
				if (cs_out.GetLength()) cs_out += _T("\r\n"); cs_out += _lp_sz_msg;
				ctl_out.SetWindowText((LPCTSTR)cs_out);

				this->UpdateScroll();

			}
		}

		VOID   UpdateScroll(void) {
			WTL::CEdit ctl_out = Lay_(m_page_ref) << This_Ctl::e_ctl_stat;
			if (ctl_out) {
				const INT vis_lines = 6;  // depends on control height and font size; is set statically for this version;
				const INT cnt_lines = ctl_out.GetLineCount();

				if (vis_lines < cnt_lines)
					ctl_out.SendMessage(EM_LINESCROLL, 0, cnt_lines - vis_lines);
			}
		}

	private:
		CStringW&   _get_int_buffer(void) {
			static CStringW cs_buff;
			return cs_buff;
		}
	};
}}}}

using namespace file::touch::gui::details;
/////////////////////////////////////////////////////////////////////////////

CPageState_Out:: CPageState_Out(void) { m_error << __MODULE__ << S_OK; }
CPageState_Out::~CPageState_Out(void) {}

/////////////////////////////////////////////////////////////////////////////

VOID  CPageState_Out::Do(void) {
	SAFE_LOCK(m_guard);
	if (NULL == m_page)
		return;
	CPageState_Fmt fmt_(m_page);
	if (m_error) {
		fmt_.Format(m_error);
	} else 
	if (m_data.Path())  {
		fmt_.Format(m_data);
	} else 
	if (m_info.Path())  {
		fmt_.Format(m_info);
	}
}

CSyncObject& CPageState_Out::Lock(void) { return m_guard; }

/////////////////////////////////////////////////////////////////////////////

CPageState_Out&   CPageState_Out::operator << (const HWND _h_page)    { SAFE_LOCK(m_guard);
	m_page = _h_page; return *this;
}
CPageState_Out&   CPageState_Out::operator << (TErrorRef  _err   )    { SAFE_LOCK(m_guard);

	m_error = _err;
	if (m_page) {
		CPageState_Fmt(m_page).Format(m_error);
	}
	return *this;
}
CPageState_Out&   CPageState_Out::operator << (const CFileData& _dat) { SAFE_LOCK(m_guard);

	m_data  = _dat; m_error = S_OK; // removes the last error;
	if (m_page) {
		CPageState_Fmt(m_page).Format(m_data);
	}
	return *this;
}
CPageState_Out&   CPageState_Out::operator << (const CFileInfo& _inf) { SAFE_LOCK(m_guard);

	m_info  = _inf; m_error = S_OK; // removes the last error;
	if (m_page) {
		CPageState_Fmt(m_page).Format(m_info);
	}
	return *this;
}

/////////////////////////////////////////////////////////////////////////////

CPageState:: CPageState(ITabSetCallback& _set_snk) : TPage(IDD_FILE_TOUCH_PAG_STATE, *this, _set_snk) {}
CPageState::~CPageState(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT    CPageState::OnPageClose(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageClose(uMsg, wParam, lParam, bHandled);
#if(0)
	CPageState_Init init_(*this, m_cf_buf);
	CPageState_Handler hand_(*this, m_cf_buf);
#endif
	return l_res;
}

LRESULT    CPageState::OnPageInit (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageInit(uMsg, wParam, lParam, bHandled);

	CPageState_Layout lay_(*this, TPage::m_set_snk);
	lay_.OnCreate();

	CPageState_Init init_(*this);
	init_.OnCreate();

	m_output << *this;
	m_output.Do();

	TPage::m_bInited = true;
	return l_res;
}

LRESULT    CPageState::OnPageSize (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;

	const LRESULT l_res = TPage::OnPageSize(uMsg, wParam, lParam, bHandled);

	CPageState_Layout layout_(*this, TPage::m_set_snk);
	layout_.OnSize();

	return l_res;
}

/////////////////////////////////////////////////////////////////////////////

#define WM_WINDOWVIS   WM_WINDOWPOSCHANGED
#define WM_POSTUPDATE (WM_USER + 1) // unfortunately, getting main window visibility on startup does not draw all controls;
                                    // before showing new scan modal dialog; we need postponing this page controls update ;

LRESULT    CPageState::TabPage_OnEvent(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (uMsg)
	{
	case WM_COMMAND: {
			if (!TPage::m_bInited)
				break;
			const WORD wNotify = HIWORD(wParam);
			const WORD ctrlId  = LOWORD(wParam);

			CPageState_Handler handler_(*this);

			if (bHandled == FALSE)
				bHandled = handler_.OnCommand(ctrlId, wNotify);
			if (bHandled == TRUE ) {
				if (This_Ctl::e_btn_refs == ctrlId) {
					SAFE_LOCK(m_output.Lock());
					CPageState_Fmt fmt_out(*this); fmt_out.Refresh();
				}
			}
		} break;
	case WM_WINDOWVIS: {
			// this routine must not handle this event,
			// otherwise, dialog page does not receive changing size message;
			LPWINDOWPOS const pPos = reinterpret_cast<LPWINDOWPOS>(lParam);
			if (true || NULL != pPos && (SWP_SHOWWINDOW & (*pPos).flags)) {
				TPage::PostMessage(WM_POSTUPDATE);
			}
		} break;
	case WM_POSTUPDATE: {
			bHandled = TRUE;
		}
	}
	return 0;
}

static
LPCTSTR lp_sz_evt_pat = _T("Event info:\n\t\tpath: %s;\n\t\tstatus=%s;");
/////////////////////////////////////////////////////////////////////////////

HRESULT    CPageState::ITimer_OnError(const CError& _err) {
	m_output << _err;
	HRESULT hr_ = S_OK;
	return  hr_;
}
HRESULT    CPageState::ITimer_OnEvent(const CFileData& _data) {

	m_output << _data ;

	HRESULT hr_ = S_OK;
	return  hr_;
}
HRESULT    CPageState::ITimer_OnEvent(const CFileInfo& _info) {

	m_output << _info ;

	HRESULT hr_ = S_OK;
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

bool       CPageState::IsChanged   (void) const {
	return false;
}

CStringW CPageState::GetPageTitle(void) const {  static CStringW cs_title(_T(" General ")); return cs_title; }

VOID       CPageState::UpdateData  (const DWORD _opt) {
	_opt;
}