#ifndef _FWDATAFILTERDLGTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED
#define _FWDATAFILTERDLGTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Jan-2016 at 3:42:18pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Watcher (thefileguardian.com) Desktop Data Filter Dailog Tab Set class declaration file.
	-----------------------------------------------------------------------------
	Adopted to USB Drive Detective (bitsphereinc.com) on 17-Aug-2018 at 6:57:00p, UTC+7, Novosibirsk, Friday;
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:57:52p, UTC+7, Novosibirsk, Monday;
	Adopted to File Touch project on 12-Feb-2020 at 3:18:31a, UTC+7, Novosibirsk, Wednesday;
*/
#include "shared.gui.page.base.h"
#include "file.touch.pag.state.h"

#include "file.touch.timer.h"

namespace file { namespace touch { namespace gui {

	using shared::gui::ITabSetCallback;

	using file::touch::biz::ITimerEventSink;

	class CTabSet
	{
	public:
		enum _pages : INT {
			e_state = 0,
			e_cfg   = 1,
		};
	private:
		WTL::CTabCtrl m_cTabCtrl;
		_pages        m_e_active;

	private: //tab page(s)
		CPageState    m_pg_state;

	public:
		 CTabSet (ITabSetCallback&);
		~CTabSet (void);

	public:
		HRESULT       Create  (const HWND hParent, const RECT& rcArea);
		HRESULT       Destroy (void);
		_pages        Selected(void) const;
		HRESULT       Selected(const _pages);
		VOID          UpdateLayout(void);
	public:
		ITimerEventSink& GetSink(void) ;
	};
}}}

#endif/*_FWDATAFILTERDLGTABSET_H_CBE4D90A_5BEA_4324_B7CA_C92D7BEB1FC7_INCLUDED*/