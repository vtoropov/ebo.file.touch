/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Dec-2018 at 6:16:09p, UTC+7, Novosibirsk, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app scan dialog interface implementation file.
	-----------------------------------------------------------------------------
	Adopted to File Touch project desktop app on 11-Feb-2020 at 11:56:31p, UTC+7, Novosibirsk, Tuesday;
*/
#include "StdAfx.h"
#include "file.touch.dlg.main.h"
#include "file.touch.res.h"

using namespace file::touch::gui;

#include "shared.gui.aux.h"
#include "shared.gui.page.layout.h"

using namespace shared::gui;
using namespace shared::gui::layout;

#include "shared.gui.gdi.plus.h"

using namespace shared::gui::draw;

/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch { namespace gui { namespace _impl {

	class CMainDlg_Ctl {
	public:
		enum _ctl : WORD {
			e_dlg_ban = IDC_FILE_TOUCH_DLG_MAIN_BAN,
		};
	};
	typedef CMainDlg_Ctl This_Ctl;

	class CMainDlg_Initer {
	private:
		CWindow&     m_dlg_ref;

	public:
		CMainDlg_Initer(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}

	public:
		HRESULT   OnCreate(void) {

			HRESULT hr_ = S_OK;

			::WTL::CStatic  w_ban = Lay_(m_dlg_ref) <<  This_Ctl::e_dlg_ban;
			if (0 != w_ban) {
				HBITMAP h_ban = NULL;
				hr_ = CGdiPlusPng_Loader().LoadImage(This_Ctl::e_dlg_ban, h_ban);
				if (SUCCEEDED(hr_)) {
					w_ban.SetBitmap(h_ban);
					::DeleteObject (h_ban); h_ban = NULL;
				}
			}

			return hr_;
		}
	};

	class CMainDlg_Layout {
	private:
		enum _e : UINT {
			e_gap   = 0x04,
			e_ban_h =   60,  // height of the ban picture in pixels (taken from image file);
			e_ban_w =  750,  // width  of the ban picture in pixels (taken from image file);
		};

	private:
		CWindow&     m_dlg_ref;
		RECT         m_dlg_rec;

	public:
		CMainDlg_Layout(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {

			if (m_dlg_ref.GetClientRect(&m_dlg_rec) == FALSE)
				::SetRectEmpty(&m_dlg_rec);
		}

	public:
		RECT     BanArea(VOID) {
			RECT rc_ban = m_dlg_rec;
			rc_ban.bottom = CMainDlg_Layout::e_ban_h;

			return /*Lay_(m_dlg_ref) = This_Ctl::e_dlg_ban*/rc_ban;
		}

		RECT     TabArea(VOID) {
			const
			RECT rc_ban = this->BanArea();
			RECT rc_btn = Lay_(m_dlg_ref) = IDOK;

			CWindow bt_okay = Lay_(m_dlg_ref) << IDOK;

			RECT rc_tab = {0};
			::SetRect(
				&rc_tab,
				m_dlg_rec.left  + _e::e_gap,
				rc_ban.bottom   + _e::e_gap / _e::e_gap,
				m_dlg_rec.right - _e::e_gap, rc_btn.top - _e::e_gap
			);

			return rc_tab;
		}
	};

	class CMainDlg_Handler {
	private:
		CWindow&     m_dlg_ref;

	public:
		CMainDlg_Handler(CWindow&  _dlg_ref) : m_dlg_ref(_dlg_ref) {}

	public:
		VOID   OnCommand(void) {

			if (true) {
				::EndDialog(m_dlg_ref, IDCANCEL);
			}
		}

	};

}}}}

using namespace file::touch::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CMainDlg::CMainDlgImpl:: CMainDlgImpl(void) : IDD(IDD_FILE_TOUCH_DLG_MAIN), m_tabset(*this) {}
CMainDlg::CMainDlgImpl::~CMainDlgImpl(void) {}

/////////////////////////////////////////////////////////////////////////////

LRESULT CMainDlg::CMainDlgImpl::OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	CMainDlg_Handler hand_(*this);
	hand_.OnCommand();

	return 0;
}

LRESULT CMainDlg::CMainDlgImpl::OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled = FALSE;
	m_tabset.Destroy();

	return 0;
}

LRESULT CMainDlg::CMainDlgImpl::OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	TDialog::CenterWindow();
	{
		CIconLoader loader_(IDR_FILE_TOUCH_ICO);
		TDialog::SetIcon(loader_.DetachLarge(), TRUE );
		TDialog::SetIcon(loader_.DetachSmall(), FALSE);
	}

	HRESULT hr_ = S_OK;

	CMainDlg_Initer dlg_init(*this);
	hr_ = dlg_init.OnCreate();

	CMainDlg_Layout  layout_(*this);
	const RECT rc_tabs = layout_.TabArea();

	hr_ = m_tabset.Create(*this, rc_tabs);
	if (SUCCEEDED(hr_)) {
	}

	return 0;
}

LRESULT CMainDlg::CMainDlgImpl::OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled) {
	uMsg; wParam; lParam; bHandled;
	switch (wParam)  {
	case VK_RETURN : {
			CMainDlg_Handler hand_(*this); hand_.OnCommand();
		} break;
	}
	return 0;
}

LRESULT CMainDlg::CMainDlgImpl::OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled)
{
	uMsg; wParam; lParam; bHandled = FALSE;
	switch (wParam) {
	case SC_CLOSE:  {
			TDialog::EndDialog(IDCANCEL);
		} break;
	}
	return 0;
}

/////////////////////////////////////////////////////////////////////////////

HRESULT   CMainDlg::CMainDlgImpl::TabSet_OnDataChanged (const UINT pageId, const bool bChanged ) {
	pageId; bChanged;
	HRESULT hr_ = S_OK;

	CWindow bt_app = TDialog::GetDlgItem(IDOK);
	if (NULL != bt_app) {
	}

	return  hr_;
}

HRESULT   CMainDlg::CMainDlgImpl::TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) {
	ctrlId; _rc_ctrl;
	HRESULT hr_ = S_OK;
	switch (ctrlId) {
	case IDYES   : case IDOK:
	case IDCANCEL:
	{
		CWindow ctl_btn = TDialog::GetDlgItem(ctrlId);
		if (NULL != ctl_btn) {
			ctl_btn.GetWindowRect(&_rc_ctrl);
		}
	} break;
	default:
	hr_ = DISP_E_PARAMNOTFOUND;
	}
	return  hr_;
}

/////////////////////////////////////////////////////////////////////////////

CMainDlg:: CMainDlg(void) : m_is_modal(false) {}
CMainDlg::~CMainDlg(void) {}

/////////////////////////////////////////////////////////////////////////////

HRESULT    CMainDlg::DoModal(void) {

	m_is_modal = true;
	const INT_PTR result = m_dlg.DoModal();
	m_is_modal = false;
	return (
			result == IDCANCEL ? S_FALSE : S_OK
		);
}

bool       CMainDlg::IsModal(void) const { return m_is_modal; }

/////////////////////////////////////////////////////////////////////////////

ITimerEventSink& CMainDlg::GetSink(void) { return m_dlg.m_tabset.GetSink(); }