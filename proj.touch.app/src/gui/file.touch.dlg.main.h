#ifndef _UDDGENDLGSCAN_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
#define _UDDGENDLGSCAN_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 15-Dec-2018 at 5:05:16p, UTC+7, Novosibirsk, Saturday;
	This is USB Drive Detective (bitsphereinc.com) desktop app scan dialog interface declaration file.
	-----------------------------------------------------------------------------
	Adopted to File Touch project desktop app on 11-Feb-2020 at 11:32:43p, UTC+7, Novosibirsk, Tuesday;
	
*/
#include "shared.gen.sys.err.h"
#include "file.touch.dlg.tabs.h"

#include "file.touch.timer.h"

namespace file { namespace touch { namespace gui {

	using shared::sys_core::CError;

	using file::touch::biz::ITimerEventSink;

	class CMainDlg {
	private:
		class CMainDlgImpl : public ::ATL::CDialogImpl<CMainDlgImpl>, ITabSetCallback {
		                    typedef ::ATL::CDialogImpl<CMainDlgImpl>  TDialog;
			friend class CMainDlg;
		private:
			CTabSet     m_tabset;

		public :
			UINT IDD;
		public:
			BEGIN_MSG_MAP(CMainDlgImpl)
				MESSAGE_HANDLER     (WM_COMMAND   ,   OnBtnCmd )
				MESSAGE_HANDLER     (WM_DESTROY   ,   OnDestroy)
				MESSAGE_HANDLER     (WM_INITDIALOG,   OnInitDlg)
				MESSAGE_HANDLER     (WM_KEYDOWN   ,   OnKeyDown) // does not work in modeless dialogs;
				MESSAGE_HANDLER     (WM_SYSCOMMAND,   OnSysCmd )
			END_MSG_MAP()

		public:
			 CMainDlgImpl (void) ;
			~CMainDlgImpl (void) ;

		private:
			LRESULT OnBtnCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnDestroy(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnInitDlg(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnKeyDown(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
			LRESULT OnSysCmd (UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);

		private: // ITabSetCallback
#pragma warning (disable: 4481)
			virtual HRESULT TabSet_OnDataChanged (const UINT pageId, const bool bChanged) override sealed;
			virtual HRESULT TabSet_OnDataRequest (const UINT ctrlId, RECT& _rc_ctrl) override sealed;
#pragma warning (default: 4481)
		};
	private:
		volatile
		bool      m_is_modal;    // required for identifying this dialog is in modal state or not;
		CMainDlgImpl   m_dlg;

	public:
		 CMainDlg (void);
		~CMainDlg (void);

	public:
		HRESULT    DoModal(void);
		bool       IsModal(void) const;
		
	public:
		ITimerEventSink& GetSink(void);
	private:
		CMainDlg (const CMainDlg&);
		CMainDlg& operator= (const CMainDlg&);
	};

}}}

#endif/*_UDDGENDLGSCAN_H_7218684D_BE99_4927_9DD4_5381FF0448EF_INCLUDED*/