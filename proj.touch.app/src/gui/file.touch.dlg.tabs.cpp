/*
	Created by Tech_dog (ebontrop@gmail.com) on 16-Jan-2016 at 3:58:22pm, GMT+7, Phuket, Rawai, Saturday;
	This is File Watcher (thefileguardian.com) Desktop Data Filter Dailog Tab Set class implementation file.
	-----------------------------------------------------------------------------
	Adopted to USB Drive Detective (bitsphereinc.com) on 17-Aug-2018 at 6:57:00p, UTC+7, Novosibirsk, Friday;
	Adopted to Ebo Pack Google push notification app on 9-Sep-2019 at 10:57:52p, UTC+7, Novosibirsk, Monday;
	Adopted to File Touch project on 12-Feb-2020 at 3:57:55a, UTC+7, Novosibirsk, Wednesday;
*/
#include "StdAfx.h"
#include "file.touch.dlg.tabs.h"
#include "file.touch.res.h"

using namespace file::touch::gui;

/////////////////////////////////////////////////////////////////////////////

namespace file { namespace touch { namespace gui { namespace _impl
{
	class CTabSet_Layout
	{
	private:
		RECT    m_area;
	public:
		CTabSet_Layout(const RECT& rcArea) : m_area(rcArea) {}

	public:
		RECT    GetTabsArea(void)const {
			RECT rcTabs = m_area;
			::InflateRect(&rcTabs, -5, -3);
			return rcTabs;
		}
	};
}}}}

using namespace file::touch::gui::_impl;
/////////////////////////////////////////////////////////////////////////////

CTabSet:: CTabSet(ITabSetCallback& _snk) : m_e_active(_pages::e_state), m_pg_state(_snk) { _snk; }
CTabSet::~CTabSet(void) { }

/////////////////////////////////////////////////////////////////////////////

HRESULT       CTabSet::Create(const HWND hParent, const RECT& rcArea) {
	hParent; rcArea;
	HRESULT hr_ = S_OK;

	if(FALSE == ::IsWindow(hParent) || ::IsRectEmpty(&rcArea))
		return (hr_ = E_INVALIDARG);

	CTabSet_Layout layout(rcArea);
	RECT rcTabs  = layout.GetTabsArea();

	m_cTabCtrl.Create(
			hParent,
			rcTabs ,
			0,
			WS_CHILD|WS_VISIBLE|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,
			WS_EX_CONTROLPARENT,
			IDC_FILE_TOUCH_DLG_MAIN_TAB
		);

	const SIZE szPadding = {
		5, 2
	};

	m_cTabCtrl.SetFont(::ATL::CWindow(hParent).GetFont());
	m_cTabCtrl.SetPadding(szPadding);
	m_cTabCtrl.SetItemSize(128,  24);

	INT nIndex = 0; nIndex;
	const DWORD dwFlags = 
		SWP_NOSIZE|SWP_NOMOVE|SWP_HIDEWINDOW; // auto-placement is made by default implementation of create/initialize page handler;

	m_cTabCtrl.AddItem(m_pg_state.GetPageTitle());
	{
		m_pg_state.Create(m_cTabCtrl.m_hWnd);
		m_pg_state.SetWindowPos(
				HWND_TOP,
				0, 0, 0 , 0,
				dwFlags
			);
		if (m_pg_state.IsWindow()) m_pg_state.Index(nIndex++);
	}
	m_cTabCtrl.SetCurSel(m_e_active);

	this->UpdateLayout();

	return S_OK;
}

HRESULT       CTabSet::Destroy(void)
{
	if (m_pg_state.IsWindow()) { m_pg_state.DestroyWindow(); m_pg_state.m_hWnd = NULL; }
	if (m_cTabCtrl.IsWindow()) { m_cTabCtrl.DestroyWindow(); m_cTabCtrl.m_hWnd = NULL; }
	return S_OK;
}

CTabSet::_pages
              CTabSet::Selected(void) const      { return m_e_active; }
HRESULT       CTabSet::Selected(const _pages _v) {
	HRESULT hr_ = S_OK;
	const bool b_changed = (_v != m_e_active);
	m_e_active  = _v;
	if (b_changed && m_cTabCtrl.IsWindow()) {
		m_cTabCtrl.SetCurSel(m_e_active);
		this->UpdateLayout();
	}
	return  hr_;
}

void          CTabSet::UpdateLayout(void)
{
	const INT nTabIndex = m_cTabCtrl.GetCurSel(); nTabIndex;
	
	if (m_pg_state.IsWindow()) m_pg_state.ShowWindow(nTabIndex == m_pg_state.Index() ? SW_SHOW : SW_HIDE);
}

/////////////////////////////////////////////////////////////////////////////

ITimerEventSink& CTabSet::GetSink(void) { return m_pg_state; }