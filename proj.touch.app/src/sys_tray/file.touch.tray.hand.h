#ifndef _TMONSYSTRAY_H_3CD25026_BECB_4991_AD54_9DEB77C97012_INCLUDED
#define _TMONSYSTRAY_H_3CD25026_BECB_4991_AD54_9DEB77C97012_INCLUDED
/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Jan-2016 at 0:13:49am, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring antivirus system tray wrapper class declaration file.
	-----------------------------------------------------------------------------
	Adopted to File Touch App project on 02-Feb-2020 at 4:36:52p, UTC+7, Novosibirsk, Sunday;
*/
#include "shared.gui.tray.area.h"
#include "file.touch.dlg.main.h"

namespace file { namespace touch {

	using shared::sys_core::CError;

	using shared::gui::CNotifyTrayArea;
	using shared::gui::INotifyTrayAreaCallback;

	using file::touch::gui::CMainDlg;
	//
	// actually, this class is used as notify area events callback handler;
	//
	class CSystemTray : public INotifyTrayAreaCallback
	{
	private:
		CMainDlg&   m_frame;
		TNotifyArea m_area ;
		CError      m_error;

	public:
		 CSystemTray(CMainDlg&);
		~CSystemTray(void);

	public:
		TNotifyArea& Area  (void);
		TErrorRef    Error (void) const;

	private: // INotifyTrayAreaCallback
#pragma warning (disable: 4481)
		HRESULT  NotifyTray_OnClickEvent(const UINT eventId)       override sealed;
		HRESULT  NotifyTray_OnContextMenuEvent(const UINT eventId) override sealed;
#pragma warning (default: 4481)
	private:
		CSystemTray(const CSystemTray&);
		CSystemTray& operator= (const CSystemTray&);
	};
}}

#endif/*_TMONSYSTRAY_H_3CD25026_BECB_4991_AD54_9DEB77C97012_INCLUDED*/