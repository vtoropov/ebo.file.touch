/*
	Created by Tech_dog (ebontrop@gmail.com) on 18-Jan-2016 at 0:19:29am, GMT+7, Phuket, Rawai, Monday;
	This is Total Monitoring antivirus system tray wrapper class implementation file.
	-----------------------------------------------------------------------------
	Adopted to File Touch app project on 02-Feb-2020 at 4:36:52p, UTC+7, Novosibirsk, Sunday;
*/
#include "StdAfx.h"
#include "file.touch.tray.hand.h"
#include "file.touch.res.h"

using namespace file::touch;

/////////////////////////////////////////////////////////////////////////////

CSystemTray:: CSystemTray(CMainDlg& _frm) : m_area(*this, 1), m_frame(_frm) { m_error << __MODULE__ << S_OK;
	
	HRESULT hr_ = m_area.Turn(true);
	if (FAILED(hr_))
		m_error = m_area.Error();
}

CSystemTray::~CSystemTray(void) {
	m_area.Turn(false);
}

/////////////////////////////////////////////////////////////////////////////

TNotifyArea& CSystemTray::Area  (void)       { return m_area ; }
TErrorRef    CSystemTray::Error (void) const { return m_error; }

/////////////////////////////////////////////////////////////////////////////
#ifndef  WM_SHOWMAINFORM
#define  WM_SHOWMAINFORM (WM_USER + 1)
#endif
/////////////////////////////////////////////////////////////////////////////

HRESULT  CSystemTray::NotifyTray_OnClickEvent(const UINT eventId) {
	eventId;
#if (0)
	CWindow host_ = m_frame;

	if (host_.IsWindow())
	{
		if (host_.IsWindowVisible() == FALSE)
			host_.SendMessage(WM_SHOWMAINFORM);
		else
			host_.SetActiveWindow();
	}
#endif
	HRESULT hr_ = S_OK;
	return  hr_;
}

HRESULT  CSystemTray::NotifyTray_OnContextMenuEvent(const UINT eventId) {
	eventId;
	HRESULT hr_ = S_OK;
	POINT   pt_ = {0};
		::GetCursorPos(&pt_);

	CMenuHandle mnu_loader;
	mnu_loader.LoadMenu(IDM_FILE_TOUCH_TRAY_SHORTCUT);
	if (mnu_loader.IsMenu() == FALSE)
		return (hr_ = __DwordToHresult(ERROR_RESOURCE_NOT_FOUND));

	CMenuHandle mnu_ctx = mnu_loader.GetSubMenu(0);

	const WORD w_id     = IDC_FILE_TOUCH_TRAY_GUI_SHOW;
	const bool bEnabled = m_frame.IsModal() == false  ;

	CStringW cs_cap(
		bEnabled ? _T("Show File Touch UI") : _T("File Touch UI Is Active")
	);
	mnu_ctx.ModifyMenu(w_id, MF_BYCOMMAND, (UINT_PTR)w_id, cs_cap.GetString());
	mnu_ctx.EnableMenuItem (
		w_id, MF_BYCOMMAND|(bEnabled ? MF_ENABLED : MF_DISABLED|MF_GRAYED)
	);

	const WORD w_cmd = (WORD) mnu_ctx.TrackPopupMenu(
		TPM_LEFTALIGN|TPM_RETURNCMD, pt_.x, pt_.y, m_area.Host()
	);
	// https://docs.microsoft.com/en-us/windows/win32/api/winuser/nf-winuser-postquitmessage
	switch (w_cmd) {
	case IDC_FILE_TOUCH_TRAY_GUI_SHOW : {
			if (m_frame.IsModal() == false)
				m_frame.DoModal();
		} break;
	case IDC_FILE_TOUCH_TRAY_EXIT: {
			::PostThreadMessage(
				::GetCurrentThreadId(), WM_QUIT, 0, 0);
		} break;
	}

	mnu_ctx.DestroyMenu();

	return  hr_;
}